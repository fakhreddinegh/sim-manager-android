package paypos.tn.simmanager;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.reflect.Method;

import paypos.tn.simmanager.remote.Config;
import paypos.tn.simmanager.utils.InfoPhone;
import paypos.tn.simmanager.utils.TelephonyInfo;

public class MainActivity extends AppCompatActivity {
    public Button btn_sim1, btn_sim2;
    public InfoPhone infoPhone;
    boolean isDualSim=false;
    Button btn_ok_ip;
    ChooseSimDialog simChooser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        infoPhone=new InfoPhone(this);
        isDualSim=infoPhone.infoPhone();


       //Intent intent = new Intent(this, LoginActivity.class);
    }
    void PopUPSIMChooser()
    {
        simChooser=new ChooseSimDialog(MainActivity.this);


        simChooser.show();
        btn_sim1 =(Button)simChooser.findViewById(R.id.btn_sim1);
        btn_sim2 =(Button)simChooser.findViewById(R.id.btn_sim2);

        btn_sim1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginInten = new Intent(MainActivity.this, LoginActivity.class);
                loginInten.putExtra("IMEI",infoPhone.getIMEI(0));
                loginInten.putExtra("IMSI",infoPhone.getIMSI(0));
                startActivity(loginInten);
                simChooser.hide();
            }
        });
        btn_sim2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginInten = new Intent(MainActivity.this, LoginActivity.class);
                loginInten.putExtra("IMEI",infoPhone.getIMEI(1));
                loginInten.putExtra("IMSI",infoPhone.getIMSI(1));
                startActivity(loginInten);
                simChooser.hide();
            }
        });
    }
    void PopUPSIMChooserRegister()
    {
        simChooser=new ChooseSimDialog(MainActivity.this);


        simChooser.show();
        btn_sim1 =(Button)simChooser.findViewById(R.id.btn_sim1);
        btn_sim2 =(Button)simChooser.findViewById(R.id.btn_sim2);

        btn_sim1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerInten = new Intent(MainActivity.this, RegisterActivity.class);
                registerInten.putExtra("IMEI",infoPhone.getIMEI(0));
                registerInten.putExtra("IMSI",infoPhone.getIMSI(0));
                startActivity(registerInten);
                simChooser.hide();
            }
        });
        btn_sim2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerInten = new Intent(MainActivity.this, RegisterActivity.class);
                registerInten.putExtra("IMEI",infoPhone.getIMEI(0));
                registerInten.putExtra("IMSI",infoPhone.getIMSI(0));
                startActivity(registerInten);
                simChooser.hide();
            }
        });
    }



    public void OnClick(View v) {
        if(v.getId()==R.id.btn_main_login)
        {
            if(isDualSim)
            {
                PopUPSIMChooser();
            }
            else
            {
                Intent loginInten = new Intent(MainActivity.this, LoginActivity.class);
                loginInten.putExtra("IMEI",infoPhone.getIMEI(0));
                loginInten.putExtra("IMSI",infoPhone.getIMSI(0));
                startActivity(loginInten);
            }

        }else
        if(v.getId()==R.id.btn_main_register)
        {
            if(isDualSim)
            {
                PopUPSIMChooserRegister();
            }
            else
            {
                Intent registerInten = new Intent(MainActivity.this, RegisterActivity.class);
                registerInten.putExtra("IMEI",infoPhone.getIMEI(0));
                registerInten.putExtra("IMSI",infoPhone.getIMSI(0));
                startActivity(registerInten);
            }

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.configuration_ip){
            PopUPIPConfig();
        }
        return super.onOptionsItemSelected(item);
    }
    IPConfigDialog ipConfig;
    void PopUPIPConfig()
    {
        ipConfig=new IPConfigDialog(MainActivity.this);


        ipConfig.show();
        btn_ok_ip =(Button)ipConfig.findViewById(R.id.btn_ok_ip);
        final EditText txt_ip =(EditText) ipConfig.findViewById(R.id.txt_ip);

     btn_ok_ip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config d=new Config(MainActivity.this);
                d.setConfig(txt_ip.getText().toString());
                ipConfig.hide();
            }
        });

    }

}




























