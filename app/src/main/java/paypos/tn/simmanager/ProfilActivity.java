package paypos.tn.simmanager;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import paypos.tn.simmanager.model.User;
import paypos.tn.simmanager.remote.Session;
import paypos.tn.simmanager.rest.ApiClient;
import paypos.tn.simmanager.rest.ApiInterface;
import paypos.tn.simmanager.utils.InfoPhone;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilActivity extends AppCompatActivity {
    public InfoPhone infoPhone;

    TextView txt_profil_firstname;
    TextView txt_profil_lastname;
    TextView txt_profil_email;
    TextView txt_profil_imei;
    TextView txt_profil_imsi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        txt_profil_firstname= (TextView) findViewById(R.id.txt_profil_firstname);
        txt_profil_lastname= (TextView) findViewById(R.id.txt_profil_lastname);
        txt_profil_email= (TextView) findViewById(R.id.txt_profil_email);
        txt_profil_imei= (TextView) findViewById(R.id.txt_profil_imei);
        txt_profil_imsi= (TextView) findViewById(R.id.txt_profil_imsi);
        //Toast.makeText(this, ""+new Session(ProfilActivity.this).getUserToken(), Toast.LENGTH_SHORT).show();
        try {
            getProfilData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void loadProfilData(User user)
    {
        txt_profil_firstname.setText("FirstName : "+user.getFirstName());
        txt_profil_lastname.setText("LastName : "+user.getLastName());
        txt_profil_email.setText("Email : "+user.getEmail());
        txt_profil_imei.setText("IMEI : "+user.getImei());
        txt_profil_imsi.setText("IMSI : "+user.getImsi());
    }

    ProgressDialog pDialog;
    public void getProfilData() throws IOException {

        if (pDialog == null)
            pDialog = new ProgressDialog(ProfilActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String auth = new Session(ProfilActivity.this).getUserToken();
        String Accept = "application/json";
        String contype = "application/json";

        ApiInterface loginService = ApiClient.createAuthService(ApiInterface.class, Accept, contype,auth);
        Call<User> call = loginService.getUser();
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (response.isSuccessful()) {
                    // response.body().toString();
                    pDialog.hide();
                    Toast.makeText(ProfilActivity.this, "sucess" + response.body().getFirstName(), Toast.LENGTH_SHORT).show();
                    loadProfilData(response.body());
                }
                if (response.errorBody() != null) {
                    pDialog.hide();
                    Toast.makeText(ProfilActivity.this, "erreur de connexion " + response.message(), Toast.LENGTH_SHORT).show();
                    pDialog.setMessage("Deconnexion..");
                    pDialog.show();
                    ProfilActivity.this.finish();
                    pDialog.hide();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                pDialog.hide();
                Toast.makeText(ProfilActivity.this, "erreur", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
