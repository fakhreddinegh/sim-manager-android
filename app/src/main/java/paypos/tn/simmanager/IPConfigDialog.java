package paypos.tn.simmanager;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import paypos.tn.simmanager.remote.Config;

public class IPConfigDialog extends Dialog implements View.OnClickListener {

    public Activity c;
    public Dialog d;
    public EditText txt_ip;
    Button btn_ok_ip;

    public IPConfigDialog(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_ip_config);

        txt_ip=(EditText)findViewById(R.id.txt_ip);
        Config conf=new Config(this.getContext());
        txt_ip.setText(conf.getConfigIP());

    }

    @Override
    public void onClick(View v) {

    }




}
