package paypos.tn.simmanager.utils;

import android.content.Context;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

import paypos.tn.simmanager.MainActivity;

/**
 * Created by fakhreddine on 12/23/2017.
 */

public class InfoPhone {
    Context c;
    InfoPhone()
    {
        super();
    }
    public InfoPhone(Context c)
    {
        super();
        this.c=c;
    }
    public String getIMSI(int num)
    {
        if(num==0||num==1)
        {
            TelephonyManager telephonyManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
            int slotIndex = num;
            int subscriptionId = SubscriptionManager.from(c).getActiveSubscriptionInfoForSimSlotIndex(slotIndex).getSubscriptionId();
            try {
                Class c = Class.forName("android.telephony.TelephonyManager");
                Method m = c.getMethod("getSubscriberId", new Class[] {int.class});
                Object o = m.invoke(telephonyManager, new Object[]{subscriptionId});

                return (String) o;

            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return "";
    }

    public String getIMEI(int num)
    {
        if(num==0)
        {
            TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(c);
            return telephonyInfo.getImsiSIM1();
        }
        if(num==1)
        {
            TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(c);
            return telephonyInfo.getImsiSIM2();
        }
        return "";
    }


    public boolean infoPhone()
    {
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(c);
        return telephonyInfo.isDualSIM();

    }

}
