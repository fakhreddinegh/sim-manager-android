package paypos.tn.simmanager.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by fakhreddine on 12/23/2017.
 */

public class Utils {

    public static List<String> getRandomList(int size)
    {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        Random randomGenerator = new Random();
        while (numbers.size() < size) {

            int random = randomGenerator .nextInt(size);
            if (!numbers.contains(random)) {
                numbers.add(random);
            }
        }
        List<String> li =new ArrayList<>();
        for (Integer i:numbers) {
            li.add(i.toString());
        }
        return li;
    }
}
