package paypos.tn.simmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import paypos.tn.simmanager.model.User;
import paypos.tn.simmanager.model.UserAuth;
import paypos.tn.simmanager.remote.Session;
import paypos.tn.simmanager.rest.ApiClient;
import paypos.tn.simmanager.rest.ApiInterface;
import paypos.tn.simmanager.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextView txt_imsi;
    TextView txt_imei;
    TextView txt_code;
    EditText txt_login_email;

    Button btn_num1;
    Button btn_num2;
    Button btn_num3;
    Button btn_num4;
    Button btn_num5;
    Button btn_num6;
    Button btn_num7;
    Button btn_num8;
    Button btn_num9;
    Button btn_num0;
    Button btn_delete;

    Button btn_login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txt_imsi= (TextView) findViewById(R.id.txt_imsi);
        txt_imei= (TextView) findViewById(R.id.txt_imei);
        txt_code= (TextView) findViewById(R.id.txt_code);
        txt_login_email=(EditText) findViewById(R.id.txt_login_email);
        btn_login = (Button) findViewById(R.id.btn_login);

        fillRandomButtons();
        initTextViews();
    }
    public void fillRandomButtons()
    {
        btn_num1 = (Button) findViewById(R.id.btn_num1);
        btn_num2 = (Button) findViewById(R.id.btn_num2);
        btn_num3 = (Button) findViewById(R.id.btn_num1);
        btn_num3 = (Button) findViewById(R.id.btn_num3);
        btn_num4 = (Button) findViewById(R.id.btn_num4);
        btn_num5 = (Button) findViewById(R.id.btn_num5);
        btn_num6 = (Button) findViewById(R.id.btn_num6);
        btn_num7 = (Button) findViewById(R.id.btn_num7);
        btn_num8 = (Button) findViewById(R.id.btn_num8);
        btn_num9 = (Button) findViewById(R.id.btn_num9);
        btn_num0 = (Button) findViewById(R.id.btn_num0);
        btn_delete = (Button) findViewById(R.id.btn_delete);

        List<String> li= Utils.getRandomList(10);

        btn_num0.setText(li.get(0));
        btn_num1.setText(li.get(1));
        btn_num2.setText(li.get(2));
        btn_num3.setText(li.get(3));
        btn_num4.setText(li.get(4));
        btn_num5.setText(li.get(5));
        btn_num6.setText(li.get(6));
        btn_num7.setText(li.get(7));
        btn_num8.setText(li.get(8));
        btn_num9.setText(li.get(9));
    }
    public void initTextViews()
    {
        txt_imei.setText("IMEI : "+getIntent().getStringExtra("IMEI"));
        txt_imsi.setText("IMSI : "+getIntent().getStringExtra("IMSI"));
    }

    public void addNumberToCode(String num)
    {
        txt_code.setText(txt_code.getText().toString()+num);
    }

    private static String removeLastChar(String str) {
        if(str.length()>1)
        return str.substring(0, str.length() - 1);
        return "";
    }
    public void OnClick(View v) {
        if(v.getId()==R.id.btn_delete)
        {
            txt_code.setText(removeLastChar(txt_code.getText().toString()));
        }else
        if(v.getId()==R.id.btn_login)
        {
            try {
                if(new Session(LoginActivity.this).authentificate(txt_login_email.getText().toString(),txt_code.getText().toString()))
                {
                    Intent loginInten = new Intent(LoginActivity.this, ProfilActivity.class);
                    startActivity(loginInten);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else{
            Button btns = (Button) findViewById(v.getId());
            String label=btns.getText().toString();
            switch (label)
            {
                case "0" : addNumberToCode("0");break;
                case "1" : addNumberToCode("1");break;
                case "2" : addNumberToCode("2");break;
                case "3" : addNumberToCode("3");break;
                case "4" : addNumberToCode("4");break;
                case "5" : addNumberToCode("5");break;
                case "6" : addNumberToCode("6");break;
                case "7" : addNumberToCode("7");break;
                case "8" : addNumberToCode("8");break;
                case "9" : addNumberToCode("9");break;
            }
        }

    }



}
