package paypos.tn.simmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.sql.Ref;
import java.util.List;

import paypos.tn.simmanager.model.User;
import paypos.tn.simmanager.rest.ApiClient;
import paypos.tn.simmanager.rest.ApiInterface;
import paypos.tn.simmanager.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    EditText txt_regis_firstname;
    EditText txt_regis_lastname;
    EditText txt_regis_email;
    TextView txt_regis_imei;
    TextView txt_regis_imsi;
    TextView txt_regis_code;

    Button btn_num1;
    Button btn_num2;
    Button btn_num3;
    Button btn_num4;
    Button btn_num5;
    Button btn_num6;
    Button btn_num7;
    Button btn_num8;
    Button btn_num9;
    Button btn_num0;
    Button btn_delete;

    Button btn_register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txt_regis_firstname= (EditText) findViewById(R.id.txt_regis_firstname);
        txt_regis_lastname= (EditText) findViewById(R.id.txt_regis_lastname);
        txt_regis_email= (EditText) findViewById(R.id.txt_regis_email);
        txt_regis_imei= (TextView) findViewById(R.id.txt_regis_imei);
        txt_regis_imsi= (TextView) findViewById(R.id.txt_regis_imsi);
        txt_regis_code= (TextView) findViewById(R.id.txt_regis_code);
        btn_register = (Button) findViewById(R.id.btn_reg_register);

        fillRandomButtons();
        initTextViews();
    }

    public void initTextViews()
    {
        txt_regis_imei.setText("IMEI : "+getIntent().getStringExtra("IMEI"));
        txt_regis_imsi.setText("IMSI : "+getIntent().getStringExtra("IMSI"));
    }
    public void fillRandomButtons()
    {
        btn_num1 = (Button) findViewById(R.id.btn_reg_num1);
        btn_num2 = (Button) findViewById(R.id.btn_reg_num2);
        btn_num3 = (Button) findViewById(R.id.btn_reg_num1);
        btn_num3 = (Button) findViewById(R.id.btn_reg_num3);
        btn_num4 = (Button) findViewById(R.id.btn_reg_num4);
        btn_num5 = (Button) findViewById(R.id.btn_reg_num5);
        btn_num6 = (Button) findViewById(R.id.btn_reg_num6);
        btn_num7 = (Button) findViewById(R.id.btn_reg_num7);
        btn_num8 = (Button) findViewById(R.id.btn_reg_num8);
        btn_num9 = (Button) findViewById(R.id.btn_reg_num9);
        btn_num0 = (Button) findViewById(R.id.btn_reg_num0);
        btn_delete = (Button) findViewById(R.id.btn_reg_delete);

        List<String> li= Utils.getRandomList(10);

        btn_num0.setText(li.get(0));
        btn_num1.setText(li.get(1));
        btn_num2.setText(li.get(2));
        btn_num3.setText(li.get(3));
        btn_num4.setText(li.get(4));
        btn_num5.setText(li.get(5));
        btn_num6.setText(li.get(6));
        btn_num7.setText(li.get(7));
        btn_num8.setText(li.get(8));
        btn_num9.setText(li.get(9));
    }

    public void addNumberToCode(String num)
    {
        txt_regis_code.setText(txt_regis_code.getText().toString()+num);
    }

    private static String removeLastChar(String str) {
        if(str.length()>1)
            return str.substring(0, str.length() - 1);
        return "";
    }

    public User getEnteredUser()
    {
        return new User(txt_regis_firstname.getText().toString(),
                 txt_regis_lastname.getText().toString(),
                 txt_regis_email.getText().toString(),
                 txt_regis_code.getText().toString(),
                 txt_regis_imei.getText().toString(),
                 txt_regis_imsi.getText().toString());
    }

    public boolean senData()
    {
        try {
            register();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    public void OnClick(View v) {
        if(v.getId()==R.id.btn_reg_delete)
        {
            txt_regis_code.setText(removeLastChar(txt_regis_code.getText().toString()));
        }else
        if(v.getId()==R.id.btn_reg_register)
        {
          if(senData())
          {
              this.finish();
          }
        }else{
            Button btns = (Button) findViewById(v.getId());
            String label=btns.getText().toString();
            switch (label)
            {
                case "0" : addNumberToCode("0");break;
                case "1" : addNumberToCode("1");break;
                case "2" : addNumberToCode("2");break;
                case "3" : addNumberToCode("3");break;
                case "4" : addNumberToCode("4");break;
                case "5" : addNumberToCode("5");break;
                case "6" : addNumberToCode("6");break;
                case "7" : addNumberToCode("7");break;
                case "8" : addNumberToCode("8");break;
                case "9" : addNumberToCode("9");break;
            }
        }

    }

    ProgressDialog pDialog;
    public void register() throws IOException {

        if (pDialog == null)
            pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String Accept = "application/json";
        String contype = "application/json";

        ApiInterface loginService = ApiClient.createService(ApiInterface.class, Accept, contype);
        Call<User> call = loginService.register(getEnteredUser());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (response.isSuccessful()) {
                    // response.body().toString();
                    pDialog.hide();
                    Toast.makeText(RegisterActivity.this, "Vous êtes enregistrer avec succée", Toast.LENGTH_SHORT).show();
                }
                if (response.errorBody() != null) {
                    pDialog.hide();
                    Toast.makeText(RegisterActivity.this, "erreur de reponse de serveur" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                pDialog.hide();
                Toast.makeText(RegisterActivity.this, "erreur", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
