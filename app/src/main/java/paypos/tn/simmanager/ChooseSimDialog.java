package paypos.tn.simmanager;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class ChooseSimDialog extends Dialog implements android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button btn_sim1, btn_sim2;

    public ChooseSimDialog(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_sim);

        btn_sim1 = (Button) findViewById(R.id.btn_sim1);
        btn_sim2 = (Button) findViewById(R.id.btn_sim2);

        btn_sim1.setOnClickListener(this);
        btn_sim2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {


    }




}
