package paypos.tn.simmanager.remote;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by User-Dell on 28/11/2016.
 */

public class Config {
    Context c;
    public Config()
    {
       super();
    }
    public Config(Context c)
    {
        super();
        this.c=c;
    }
    public static final String PREFS_IP_SERVER = "prefs_ip_server";

    public String getConfigIP()
    {
        SharedPreferences preferences = c.getSharedPreferences(PREFS_IP_SERVER, c.MODE_PRIVATE);
        String ip_server = preferences.getString("ip_server", null);

        if (ip_server != null){
            return  ip_server;
        }else
        {
            setConfig("192.168.1.3:88/sim-manager-backend/public/");
            return getConfigIP();
        }
    }
    public void setConfig(String ip_server)
    {
        SharedPreferences.Editor editor = c.getSharedPreferences(PREFS_IP_SERVER, c.MODE_PRIVATE).edit();
        editor.putString("ip_server", ip_server);
        editor.commit();
    }
}
