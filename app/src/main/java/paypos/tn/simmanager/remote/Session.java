package paypos.tn.simmanager.remote;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.io.IOException;

import paypos.tn.simmanager.LoginActivity;
import paypos.tn.simmanager.ProfilActivity;
import paypos.tn.simmanager.model.User;
import paypos.tn.simmanager.model.UserAuth;
import paypos.tn.simmanager.rest.ApiClient;
import paypos.tn.simmanager.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User-Dell on 24/11/2016.
 */

public class Session {
    Context c;
    public Session()
    {
        super();
    }
    public Session(Context c)
    {
        super();
        this.c=c;
    }
    public static final String PREFS_USER_TOKEN = "prefs_user_token";

    public String getUserToken()
    {
        SharedPreferences preferences = c.getSharedPreferences(PREFS_USER_TOKEN, c.MODE_PRIVATE);
        String token = preferences.getString("token", null);

        if (token != null){
            return  token;
        }else
        {
            return null;
        }
    }
    public void setUserToken(String token)
    {
        SharedPreferences.Editor editor = c.getSharedPreferences(PREFS_USER_TOKEN, c.MODE_PRIVATE).edit();
        editor.putString("token", token);
        editor.commit();
    }

    ProgressDialog pDialog;
    public boolean authResponse;
    public User currentUser;
    public boolean authentificate(String email,String password) throws IOException {

        if (pDialog == null)
            pDialog = new ProgressDialog(c);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String Accept = "application/json";
        String contype = "application/json";

        ApiInterface loginService = ApiClient.createService(ApiInterface.class, Accept, contype);
        Call<UserAuth> call = loginService.Authentificate("password",2,"8fSfiaN2j2yTdMmShwxbIyzj12Cxffe83XS8pUku",email,password,"*");
        call.enqueue(new Callback<UserAuth>() {
            @Override
            public void onResponse(Call<UserAuth> call, Response<UserAuth> response) {
                if (response.isSuccessful()) {
                    pDialog.hide();

                    String token=response.body().getAccess_token();
                    new Session(c).setUserToken("Bearer "+ token);
                    authResponse=true;
                    Intent loginInten = new Intent(c, ProfilActivity.class);
                    c.startActivity(loginInten);
                }
                if (response.errorBody() != null) {
                    pDialog.hide();
                    authResponse=false;
                    Toast.makeText(c, "erreur de reponse de serveur" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserAuth> call, Throwable t) {
                pDialog.hide();
                Toast.makeText(c, "erreur", Toast.LENGTH_SHORT).show();
            }
        });
        return authResponse;
    }

    public User getProfilData() throws IOException {

        if (pDialog == null)
            pDialog = new ProgressDialog(c);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String auth = new Session(c).getUserToken();
        String Accept = "application/json";
        String contype = "application/json";

        ApiInterface loginService = ApiClient.createAuthService(ApiInterface.class, Accept, contype,auth);
        Call<User> call = loginService.getUser();
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if (response.isSuccessful()) {
                    // response.body().toString();
                    pDialog.hide();
                  //  currentUser=new User();

                    Session.this.currentUser= response.body();
                  //  Toast.makeText(c, "sucess" + response.body().getFirstName(), Toast.LENGTH_SHORT).show();
                    //loadProfilData(response.body());
                }
                if (response.errorBody() != null) {
                    pDialog.hide();
                    Toast.makeText(c, "erreur de connexion " + response.message(), Toast.LENGTH_SHORT).show();
                    pDialog.setMessage("Deconnexion..");
                    currentUser=new User();

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                pDialog.hide();
                Toast.makeText(c, "erreur", Toast.LENGTH_SHORT).show();
            }
        });
        return Session.this.currentUser;
    }


}
