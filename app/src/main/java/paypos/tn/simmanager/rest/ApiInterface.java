package paypos.tn.simmanager.rest;

import paypos.tn.simmanager.model.User;
import paypos.tn.simmanager.model.UserAuth;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET("api/user")
    Call<User> getUser();

    @POST("oauth/token")
    @FormUrlEncoded
    Call<UserAuth> Authentificate(
                              @Field("grant_type") String grant_type,
                              @Field("client_id") int client_id,
                              @Field("client_secret") String client_secret,
                              @Field("username") String username,
                              @Field("password") String password,
                              @Field("scope") String scope);

   @POST("api/register")
   Call<User> register(@Body User user);
}
