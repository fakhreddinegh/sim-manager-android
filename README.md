# SimManager
![SIM Manager](https://i.imgur.com/bOt1bCS.png)

## Features

* read IMEI and IMSI
* connect to Laravel api web service via Retrofit 2 
* Send GET & POST request to get authenticate with acces-token to ensure security

## Tutorials:

![SIM Manager](https://i.imgur.com/Efpblpn.png)
![SIM Manager](https://i.imgur.com/UDuHVWc.png)
![SIM Manager](https://i.imgur.com/TFfShPO.png)
![SIM Manager](https://i.imgur.com/A0OPFhE.png)
